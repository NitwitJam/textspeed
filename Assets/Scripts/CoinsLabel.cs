﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class CoinsLabel : MonoBehaviour {
    Text textComponent;

	// Use this for initialization
	void Start () {
        GameControl.CoinsDidChangeEvent += coinsDidChange;
        textComponent = this.GetComponent<Text>();
        textComponent.text = GameControl.instance.Coins.ToString();
    }
	
    void OnDestroy()
    {
        GameControl.CoinsDidChangeEvent -= coinsDidChange;
    }

	// Update is called once per frame
	void Update () {
		
	}

    void coinsDidChange()
    {
        textComponent.text = GameControl.instance.Coins.ToString();
    }
}
