﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Runner : ARunner {
    
    int totalPoints;
    int pointsGainedForWayPoint;
    
    // should use a real "isRunning", and isRunning should become isInRace or isPaused or smth
    Vector3 exactTargetPosition;
    // Use this for initialization
    override protected IEnumerator Start () {
        yield return base.Start();
        exactTargetPosition = originalPosition;
    }
	
	// Update is called once per frame
	protected void Update () {
        Debug.DrawLine(originalPosition, originalPosition + new Vector3(0, 100, 0), Color.blue);
        if (!isInitialized)
        {
            return;
        }
        float step = 300 * Time.deltaTime;
        Debug.DrawLine(rectTransform.position, exactTargetPosition, Color.red);
        Debug.DrawLine(originalPosition, endPoint.transform.position, Color.yellow);
        rectTransform.position = Vector3.MoveTowards(rectTransform.position, exactTargetPosition, step);
        if (rectTransform.position == endPoint.transform.position)
        {
            onReachEndLine();
            print("end level");
        }
    }

    override public bool isLocal()
    {
        return true;
    }

    override public void Advance(int points)
    {
        if (!isInitialized)
        {
            return;
        }
        //points = 100;
        if (!isRunning)
        {
            return;
        }
        int pointsForNextWayPointBefore = endPoint.pricePointsToReach - pointsGainedForWayPoint;
        int pointsForNextWayPointAfter = pointsForNextWayPointBefore - points;
        if (pointsForNextWayPointAfter <= 0)
        {
            isRunning = false;
            exactTargetPosition = endPoint.transform.position;
        }
        else
        {
            pointsGainedForWayPoint += points;
        }
        totalPoints += points;
        //print("total Points: " + totalPoints);
        if (isRunning)
        {
            float targetPositionPercent = (float)pointsGainedForWayPoint / (float)endPoint.pricePointsToReach;
            Vector3 wayPointOrigin = originalPosition;
            Vector3 wayPointDestination = endPoint.transform.position;
            exactTargetPosition = wayPointOrigin + (wayPointDestination - wayPointOrigin) * targetPositionPercent;
        }
    }

    override public void TeleportToBeginning()
    {
        base.TeleportToBeginning();
        exactTargetPosition = originalPosition;
        pointsGainedForWayPoint = 0;
        isRunning = true;
    }
    override public int GetPoints()
    {
        return totalPoints;
    }
}
