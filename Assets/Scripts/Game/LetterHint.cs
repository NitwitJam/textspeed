﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LetterHint : MonoBehaviour {

    char text;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetLetter(char letter)
    {
        GetComponentInChildren<Text>().text = letter.ToString();
        text = letter;
    }
}
