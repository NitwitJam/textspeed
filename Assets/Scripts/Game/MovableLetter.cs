﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

[RequireComponent(typeof(Letter), typeof(Button))]
public class MovableLetter : MonoBehaviour, IPointerDownHandler
{
    public List<RectTransform> targetRectTransforms;
    Letter componentLetter;
    Button componentButton;
    bool isMoving = false;
    bool isMaybeMoving = false;
    Vector3 originalPosition;
    Vector2 originalInputPosition;

    // Use this for initialization
    void Start () {
        componentLetter = this.GetComponent<Letter>();
        componentButton = this.GetComponent<Button>();
        
    }
	
	// Update is called once per frame
	void Update () {
        if (!isMoving)
        {

            if (isMaybeMoving) {
                Vector2 targetPosition;
                try
                {
                    targetPosition = GetTargetPosition();
                } catch (Exception e)
                {
                    print(e.Message);
                    print("should select, move to next container");
                    Select();
                    return;
                }
                if (Vector2.Distance(targetPosition, originalInputPosition) > 10)
                {
                    isMaybeMoving = false;
                    isMoving = true;
                    print("start move");
                }
            }
            return;
        }
        if (Input.touchCount > 0)
        {
            isMoving = false;
            print("place letter");
            updatePositionTo(Input.touches[0].position);
        } else if (Input.anyKey)
        {
            updatePositionTo(Input.mousePosition);
        } else
        {
            bool hasFoundParent = false;
            foreach (RectTransform rt in targetRectTransforms)
            {
                //Rect worldRect = RectTransformExt.GetWorldRect(rt, Vector2.one);
                if (RectTransformUtility.RectangleContainsScreenPoint(rt, Input.mousePosition))
                {
                    print("found parent");
                    hasFoundParent = true;
                    int numberOfFutureSiblings = rt.childCount;
                    Transform closestSibling = null;
                    for (int i = 0; i < numberOfFutureSiblings; i++)
                    {
                        Transform siblingToCompare = rt.GetChild(i).transform;
                        if (siblingToCompare.position.x < transform.position.x &&
                            (!closestSibling || closestSibling.position.x < siblingToCompare.position.x))
                        {
                            closestSibling = siblingToCompare;
                        }
                    }
                    int offset = 1;
                    if (closestSibling && rt == transform.parent && closestSibling.GetSiblingIndex() > transform.GetSiblingIndex())
                    {
                        offset = 0;
                    }
                    transform.SetParent(rt);
                    int siblingIndex = closestSibling ? closestSibling.GetSiblingIndex() + offset : 0;
                    if (rt == transform.parent && siblingIndex == transform.GetSiblingIndex())
                    {
                        transform.position = originalPosition;
                    }
                    transform.SetSiblingIndex(siblingIndex);
                    break;
                }
            }
            isMoving = false;
            print("place letter");
            if (!hasFoundParent)
            {
                transform.position = originalPosition;
            }
        }

    }
    public void OnPointerDown(PointerEventData eventData)
    {
        originalPosition = this.transform.position;
        originalInputPosition = GetTargetPosition();
        isMaybeMoving = true;
        print("start maybe move");
    }

    void updatePositionTo(Vector3 position)
    {
        transform.position = position;
    }


    Vector2 GetTargetPosition()
    {
        if (Input.anyKey) {
            return new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        } else if (Input.touchCount > 0)
        {
            return Input.touches[0].position;
        }
        throw (new KeyNotFoundException("Unable to find target position"));
    }

    public void Select()
    {
        isMaybeMoving = false;
        for (int i = 0; i < targetRectTransforms.Count; i++)
        {
            if (targetRectTransforms[i] == transform.parent)
            {
                transform.SetParent(targetRectTransforms[(i + 1) % targetRectTransforms.Count]);
                transform.SetAsLastSibling();
                return;
            }
        }
    }
}
