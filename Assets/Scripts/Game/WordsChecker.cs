﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using UnityEngine;

public class WordsChecker : MonoBehaviour {
    public static int wordMaximumLength = 50;
	public TextAsset wordsList;
    bool isLoaded = false;
	int totalWords = 0;

	public int TotalWords {
		get {
			return totalWords;
		}
	}

    // authorized words are indexed onto their length, so if a word in the dictionary is longer than 50 characters, game will crash.
    // [0] will always be empty, but it's not a great lose of performance and better to read.. an accessor would fix it but let's keep codebase at minimum
    List<string>[] authorizedWords = new List<string>[wordMaximumLength + 1];

    public bool IsLoaded
    {
        get
        {
            return isLoaded;
        }
    }

	public IEnumerator Start() {
		yield return LoadWords ();
	}

    public List<string> getAuthorizedWordsForLength(int length)
    {
        return authorizedWords[length];
    }

    public IEnumerator LoadWords(bool force = false)
    {
        if (force || !IsLoaded)
        {
            this.isLoaded = false;
            authorizedWords = new List<string>[50];
			string[] lines = wordsList.text.Split ('\n');
			int i = 0;
			foreach(string line in lines)
            {
                if (authorizedWords[line.Length] == null)
                {
                    authorizedWords[line.Length] = new List<string>();
                }
                authorizedWords[line.Length].Add(line.ToLower());
                if (i % 5000 == 0)
                {
                    yield return new WaitForSeconds(0.01f);
                }
                i++;
            }
			totalWords = i;
            this.isLoaded = true;
        }
    }

    public bool ValidateWord(string word)
    {
        string wordLowerCase = word.ToLower();
        return authorizedWords[word.Length].Contains(wordLowerCase);
    }
    public string getRandomStringWithLength(int length)
    {
        return authorizedWords[length][UnityEngine.Random.Range(0, authorizedWords[length].Count)];
    }
}
