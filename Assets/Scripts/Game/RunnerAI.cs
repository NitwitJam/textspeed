﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunnerAI : ARunner {
    public int speed = 20;

    // Update is called once per frame
	void Update () {
		if (!isRunning) {
			return;
		}
        float step = speed * Time.deltaTime;
        rectTransform.position = Vector3.MoveTowards(rectTransform.position, endPoint.transform.position, step);
        if (rectTransform.position == endPoint.transform.position)
        {
            onReachEndLine();
        }
        Debug.DrawLine(originalPosition, originalPosition + new Vector3(0, 100, 0), Color.green);
    }
}
