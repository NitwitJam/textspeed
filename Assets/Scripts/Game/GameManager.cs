﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
    public const int NUMBER_OF_LETTERS = 7;

    public GameObject lettersContainer;
    public GameObject selectedLettersContainer;
    public Letter letterPrefab;
    public List<ARunner> runners;
    public Popup popupWin;
    public Popup popupLose;

    public Text scoreText;

	public delegate void GameDidValidateWord(string wordValidated);
	public event GameDidValidateWord GameDidValidateWordEvent;
	public delegate void GameDidUpdateLoadingProgress(float progress);
	public event GameDidUpdateLoadingProgress GameDidUpdateLoadingProgressEvent;


    List<Letter> letters = new List<Letter>();

    List<string> acceptableWords = new List<string>();
    List<string> forbiddenWords = new List<string>();
    List<string> wordsLeftToGuess = new List<string>();

    bool lettersShown = false;

	float loadingProgress = 0;
	public float LoadingProgress {
		get {
			return loadingProgress;
		}
	}

    // Use this for initialization
    IEnumerator Start () {
        yield return InitNewLevel();
    }

    void Update()
    {
        scoreText.text = runners[0].GetPoints().ToString();
    }

    void runnerDidReachEndLine(ARunner r)
    {
        if (r == runners[0])
        {
            GameControl.instance.Coins += 100;
            popupWin.Show();
        } else
        {
            popupLose.Show();
        }
        foreach (ARunner r2 in runners)
        {
            r2.OnReachEndLine -= runnerDidReachEndLine;
			r2.isRunning = false;
			r2.TeleportToBeginning();
        }
    }

	void resetLoadingProgress() {
		loadingProgress = 0;
		if (GameDidUpdateLoadingProgressEvent != null)
		{
			GameDidUpdateLoadingProgressEvent(loadingProgress);
		}
	}

	void increaseLoadingProgress(float progress, float max = 1) {
		if (loadingProgress + progress > 1) {
			loadingProgress = 1;
		}
		if (loadingProgress + progress > max) {
			loadingProgress = max;
		} else {
			loadingProgress += progress;
		}
		if (GameDidUpdateLoadingProgressEvent != null)
		{
			GameDidUpdateLoadingProgressEvent(loadingProgress);
		}
	}

    IEnumerator showNewLetters()
    {
		loadingProgress = 0;
        lettersShown = false;
        if (letters != null)
        {
            foreach (Letter l in letters)
            {
                Destroy(l.gameObject);
            }
            letters.Clear();
        }
        Dictionary<char, int> letterPoints = new Dictionary<char, int> {
            { 'a', 1 },
            { 'b', 3 },
            { 'c', 3 },
            { 'd', 2 },
            { 'e', 1 },
            { 'f', 4 },
            { 'g', 2 },
            { 'h', 4 },
            { 'i', 1 },
            { 'j', 8 },
            { 'k', 10 },
            { 'l', 1 },
            { 'm', 2 },
            { 'n', 1 },
            { 'o', 1 },
            { 'p', 3 },
            { 'q', 8 },
            { 'r', 1 },
            { 's', 1 },
            { 't', 1 },
            { 'u', 1 },
            { 'v', 4 },
            { 'w', 10 },
            { 'x', 10 },
            { 'y', 10 },
            { 'z', 10 }
        };
		string bestWord = GameControl.instance.wordsChecker.getRandomStringWithLength(NUMBER_OF_LETTERS);
        print(bestWord);
        char[] keptLetters = bestWord.ToCharArray();
        for (int i = 0; i < keptLetters.Length; i++)
        {
            char temp = keptLetters[i];
            int randomIndex = UnityEngine.Random.Range(i, keptLetters.Length);
            keptLetters[i] = keptLetters[randomIndex];
            keptLetters[randomIndex] = temp;
        }
        List<RectTransform> targetableContainers = new List<RectTransform>();
        targetableContainers.Add(lettersContainer.GetComponent<RectTransform>());
        targetableContainers.Add(selectedLettersContainer.GetComponent<RectTransform>());
        foreach (char l in keptLetters)
        {
            Letter letter = Instantiate(letterPrefab);
            letter.AddMovableComponent(targetableContainers);
            letter.SetDidSelectLetterDelegate(selectLetter);
            letter.SetLetter(l.ToString(), letterPoints[l]);
            letter.transform.SetParent(lettersContainer.transform);
            letters.Add(letter);
			increaseLoadingProgress (0.01f);
            yield return new WaitForSeconds(0.05f);
        }
        lettersShown = true;
        print("shown letters !");
		increaseLoadingProgress (0.1f);
		yield return new WaitForSeconds (0.01f);
        // TODO: This might be doable in background (yield)
		WordsChecker wc = GameControl.instance.wordsChecker;
        Dictionary<char, int> numberOfAvailableLetters = new Dictionary<char, int>();
        foreach (char l in keptLetters)
        {
            if (!numberOfAvailableLetters.ContainsKey(l))
            {
                numberOfAvailableLetters[l] = 0;
            }
            numberOfAvailableLetters[l]++;
        }
        int maximumPoints = 0;
        List<string> correctWords = new List<string>();

		float pointPerWord = ((1 - loadingProgress) / wc.TotalWords) * 1000;

        for (int i = 0; i < WordsChecker.wordMaximumLength; i++)
        {
            yield return new WaitForSeconds(0.01f);
            List<string> authorizedWordsForLength = wc.getAuthorizedWordsForLength(i);
            if (authorizedWordsForLength != null)
            {
                int j = 0;
                foreach (string authorizedWord in authorizedWordsForLength)
                {
                    if (j++ % 1000 == 0)
                    {
                        yield return new WaitForSeconds(0.01f);
						increaseLoadingProgress (pointPerWord, 0.95f);
                    }
                    Dictionary<char, int> numberOfLettersInAuthorizedWord = new Dictionary<char, int>();
                    foreach (char l in authorizedWord)
                    {
                        if (!numberOfLettersInAuthorizedWord.ContainsKey(l))
                        {
                            numberOfLettersInAuthorizedWord[l] = 0;
                        }
                        numberOfLettersInAuthorizedWord[l]++;
                    }
                    bool isCorrectWord = true;
                    foreach (char letterToCheck in authorizedWord)
                    {
                        if (!numberOfAvailableLetters.ContainsKey(letterToCheck) ||
                            numberOfAvailableLetters[letterToCheck] < numberOfLettersInAuthorizedWord[letterToCheck])
                        {
                            isCorrectWord = false;
                            break;
                        }
                    }
                    if (isCorrectWord)
                    {
						print (authorizedWord);
                        correctWords.Add(authorizedWord);
                        foreach (char l in authorizedWord)
                        {
                            maximumPoints += letterPoints[l];
                        }
                    }
                }
            }
        }
        acceptableWords = correctWords;
        wordsLeftToGuess = new List<string>(correctWords);
        print("number of correct words: " + wordsLeftToGuess.Count);
        print("maximum possible points: " + maximumPoints);
		increaseLoadingProgress (1);
    }

    void selectLetter(Letter letter)
    {
        Letter selectedLetter = GameObject.Instantiate<Letter>(letterPrefab);
        selectedLetter.SetLetter(letter.GetLetter(), letter.Points);
        selectedLetter.transform.SetParent(selectedLettersContainer.transform);
        selectedLetter.transform.SetAsLastSibling();
    }

    public void EraseLastLetter()
    {
        if (selectedLettersContainer.transform.childCount == 0)
        {
            return;
        }
        Letter[] selectedLetters = selectedLettersContainer.GetComponentsInChildren<Letter>();
        int maxSibling = -1;
        Transform lastLetter = null;
        for (int i = 0; i < selectedLettersContainer.transform.childCount; i++)
        {
            if (selectedLetters[i].transform.GetSiblingIndex() > maxSibling)
            {
                lastLetter = selectedLetters[i].transform;
            }
        }
        lastLetter.SetParent(lettersContainer.transform);
        lastLetter.SetAsLastSibling();
    }
    public void Shuffle()
    {
        Transform[] lettersToShuffle = lettersContainer.GetComponentsInChildren<Transform>();
        for (int i = 0; i < lettersContainer.transform.childCount; i++)
        {
            int letterIndex = UnityEngine.Random.Range(i, lettersContainer.transform.childCount);
            int randomSiblingIndex = UnityEngine.Random.Range(letterIndex, letterIndex + lettersContainer.transform.childCount) % (lettersContainer.transform.childCount + 1);
            lettersToShuffle[letterIndex].transform.SetSiblingIndex(randomSiblingIndex);

        }
    }
    
    public void validate()
    {
		WordsChecker wc = GameControl.instance.wordsChecker;
        String wordToValidate = "";
        Letter[] selectedLetters = selectedLettersContainer.GetComponentsInChildren<Letter>();
        int j = 0;
        int numberOfLetters = selectedLetters.Length;
        for (int i = 0; i < numberOfLetters && j < numberOfLetters; i++)
        {
            if (selectedLetters[i].transform.GetSiblingIndex() == j)
            {
                wordToValidate += selectedLetters[i].GetLetter();
                j++;
                i = 0;
            }
        }
        if (j < numberOfLetters)
        {
            print("something went bad");
            return;
        }
        if (forbiddenWords.Contains(wordToValidate.ToLower()))
        {
            print("This word is forbidden");
            return;
        }
        wordToValidate = wordToValidate.ToLower();
        print("word to validate: " + wordToValidate);
        print(string.Join(",", wordsLeftToGuess.ToArray()));
        if (wordsLeftToGuess.Exists(word => word == wordToValidate))
        {
            print("validated!");
            forbiddenWords.Add(wordToValidate);
            wordsLeftToGuess.Remove(wordToValidate);
            int gainedPoints = 0;
            foreach (Letter l in selectedLetters)
            {
                gainedPoints += l.Points;
                l.transform.SetParent(lettersContainer.transform);
            }
            runners[0].Advance(gainedPoints);
            Shuffle();
            if (GameDidValidateWordEvent != null)
            {
                GameDidValidateWordEvent(wordToValidate);
            }
        } else
        {
            print("not validated");
        }
    }

	public void InitNewLevelAction() {
		StartCoroutine(InitNewLevel());
	}

    public IEnumerator InitNewLevel()
    {
		popupWin.Hide();
        popupLose.Hide();
		print ("1");
		yield return GameControl.waitForWordsChecker();
		print ("2");
		IEnumerator e = showNewLetters();
		print ("3");
        while (lettersShown == false)
        {
            yield return e.Current;
            e.MoveNext();
        }
		print ("4");
        foreach(ARunner r in runners)
        {
            r.TeleportToBeginning();
			r.isRunning = true;
            r.OnReachEndLine += runnerDidReachEndLine;
			print ("5");
        }
        yield return e;
    }
    public void BackToMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    public List<string> GetForbiddenWords()
    {
        return forbiddenWords;
    }
    public List<string> GetWordsLeftToGuess()
    {
        return wordsLeftToGuess;
    }
}
