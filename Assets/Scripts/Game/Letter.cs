﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class Letter : MonoBehaviour {
    public delegate void DidSelectLetterDelegate(Letter letter); // declare delegate type

    protected DidSelectLetterDelegate didSelectLetterDelegate; // to store the function

    Button letterButton;
    Text childText;
    int points;
    string text;

    public int Points
    {
        get
        {
            return points;
        }
    }

    // Use this for initialization
    void Start () {
        letterButton = GetComponent<Button>();
        childText = GetComponentInChildren<Text>();
        //letterButton.onClick.AddListener(select);

        childText.text = text;
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void SetDidSelectLetterDelegate(DidSelectLetterDelegate del)
    {
        didSelectLetterDelegate = del;
    }

    public void SetLetter(string letter, int pts)
    {
        text = letter.ToUpper();
        points = pts;
    }
    public void AddMovableComponent(List<RectTransform> targetRectTransforms)
    {
        MovableLetter ml =  this.gameObject.AddComponent<MovableLetter>();
        ml.targetRectTransforms = targetRectTransforms;
    }

    public string GetLetter()
    {
        return text;
    }

    /*void select()
    {
        if (didSelectLetterDelegate == null)
        {
            return;
        }
        didSelectLetterDelegate(this);
        letterButton.interactable = false;
        print("onclick");
    }*/

    public void UnSelect()
    {
        letterButton.interactable = true;
    }
}
