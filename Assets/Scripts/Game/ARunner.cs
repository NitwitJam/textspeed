﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class ARunner : MonoBehaviour {
    protected RectTransform rectTransform;
    public WayPoint endPoint;
    public delegate void ReachEndLineDelegate(ARunner r);
    public virtual event ReachEndLineDelegate OnReachEndLine;

    protected bool isInitialized = false;

    protected Vector3 originalPosition;

	public bool isRunning = false;

    virtual protected IEnumerator Start()
    {
        rectTransform = GetComponent<RectTransform>();
        yield return new WaitForSeconds(.1f);
        originalPosition = rectTransform.position;
        isInitialized = true;
    }

    public virtual bool isLocal()
    {
        return false;
    }

    public virtual void Advance(int points)
    {

    }
    public virtual void TeleportToBeginning()
    {
        if (isInitialized)
        {
            rectTransform.position = originalPosition;
        }
    }

    public virtual int GetPoints()
    {
        return 0;
    }

    public void onReachEndLine()
    {
        if (OnReachEndLine != null)
        {
            OnReachEndLine(this);
        }
    }
}
