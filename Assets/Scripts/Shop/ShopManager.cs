﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ShopManager : MonoBehaviour {

	// Use this for initialization
	void Start() {
		
	}
	
	// Update is called once per frame
	void Update() {
		
	}

    public void buyPowerFindWord()
    {
        if (GameControl.instance.Coins >= 100)
        {
            PlayerData playerData = GameControl.instance.getPlayerData();
            playerData.powerFindWordsNumber++;
            playerData.coins -= 100;
            GameControl.instance.initWithPlayerData(playerData);
        }
    }

    public void seeVideoForCoins()
    {
        print("TODO");
    }

    public void GoToMenu()
    {
        SceneManager.LoadScene("Menu");
    }

}
