﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DailyRewardNotifier : MonoBehaviour {
    public RectTransform canvas;
    public Popup prefabPopupDailyRewardWon;
    public Popup prefabPopupDailyRewardLost;

    static bool hasNotifiedPlayer = false;

    bool isReady = false;

	// Use this for initialization
	IEnumerator Start () {
        yield return GameControl.waitForConnectionTracker();
        if (GameControl.instance.connectionTracker.numberOfConnectionsToday > 1)
        {
            // FIXME: I should save if player has been informed instead of assuming 1 connection = 1 notification
            hasNotifiedPlayer = true;
            isReady = true;
        }
    }
	
    void Awake()
    {

    }

	// Update is called once per frame
	void Update () {
        if (isReady && !hasNotifiedPlayer)
        {
            if (GameControl.instance != null && GameControl.instance.achievementDaily != null && GameControl.instance.achievementDaily.IsReady)
            {
                if (GameControl.instance.achievementDaily.currentStreak <= 1) {
                    Popup p = GameObject.Instantiate(prefabPopupDailyRewardLost, canvas, false);
                    p.Show();
                }
                else
                {
                    // initialize popup with GameControl.instance.achievementDaily.currentStreak
                    Popup p = GameObject.Instantiate(prefabPopupDailyRewardWon, canvas, false);
                    p.Show();
                }
                // TODO: put this as a boolean a bit later (after the popup closes)
                hasNotifiedPlayer = true;
            }
        }
    }
}
