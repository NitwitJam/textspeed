﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowersManager : MonoBehaviour {
    public List<ARunner> runners;
    public List<APower> powers;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Activate(APower power)
    {
        power.Activate(runners);
    }

}
