﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class ConnectionTracker : MonoBehaviour {

    public DateTime lastConnectionDate;
    public int numberOfConnectionsToday = 0;

    public bool IsReady = false;

    // TODO: save playtime

    void Start()
    {
        Load();
    }

    public void UpdateLastConnectionDate()
    {
        // FIXME: dangerous to do that, we'll lose precious information.
        this.lastConnectionDate = DateTime.Now;
        Save();
    }

    void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/connectionInfo.dat", FileMode.OpenOrCreate);
        ConnectionData playerData = new ConnectionData();
        playerData.lastConnectionDate = this.lastConnectionDate;
        playerData.numberOfConnectionsToday = this.numberOfConnectionsToday;
        bf.Serialize(file, playerData);
        file.Close();
    }

    void Load()
    {
        if (!File.Exists(Application.persistentDataPath + "/connectionInfo.dat"))
        {
            print("Can't load");
            IsReady = true;
            return;
        }
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/connectionInfo.dat", FileMode.OpenOrCreate);
        ConnectionData connectionData = (ConnectionData)bf.Deserialize(file);

        lastConnectionDate = connectionData.lastConnectionDate;
        if (lastConnectionDate.Day == DateTime.Now.Day)
        {
            this.numberOfConnectionsToday = 1;
        }
        else
        {
            this.numberOfConnectionsToday++;
        }
        bf.Serialize(file, connectionData);
        file.Close();
        IsReady = true;
    }
}


[Serializable]
class ConnectionData
{
    public DateTime lastConnectionDate;
    public int numberOfConnectionsToday;
}