﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

[RequireComponent(typeof(ConnectionTracker), typeof(AchievementDaily), typeof(WordsChecker))]
public class GameControl : MonoBehaviour {

    public delegate void CoinsDidChange();
    public static event CoinsDidChange CoinsDidChangeEvent;

    public delegate void GameControlIsReady();
    public static event GameControlIsReady GameControlIsReadyEvent;

    public static GameControl instance;

	[HideInInspector]
	public WordsChecker wordsChecker;

	[HideInInspector]
	public ConnectionTracker connectionTracker;
	[HideInInspector]
	public AchievementDaily achievementDaily;
	[HideInInspector]
    public bool IsReady = false;

    private int _coins;
    public int Coins
    {
        get
        {
            return _coins;
        }
        set
        {
            if (_coins != value)
            {
                _coins = value;
                if (CoinsDidChangeEvent != null)
                {
                    CoinsDidChangeEvent();
                }
                Save();
            }
        }
    }
    private int _powerFindWordsNumber;
    public int PowerFindWordsNumber
    {
        get
        {
            return _powerFindWordsNumber;
        }
        set
        {
            _powerFindWordsNumber = value;
            // TODO: Is an event needed ?
            Save();
        }
    }

    void Start()
    {
		
    }

	void Awake () {
		if (instance == null)
        {
            instance = this;
            connectionTracker = GetComponent<ConnectionTracker>();
            achievementDaily = GetComponent<AchievementDaily>();
			wordsChecker = GetComponent<WordsChecker>();
            Load();
            DontDestroyOnLoad(gameObject);
            IsReady = true;
        } else if (instance != this)
        {
            Destroy(this.gameObject);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.OpenOrCreate);
        PlayerData playerData = getPlayerData();
        bf.Serialize(file, playerData);
        file.Close();
    }

    public void Load()
    {
        if (!File.Exists(Application.persistentDataPath + "/playerInfo.dat"))
        {
            print("Can't load");
            return;
        }
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.OpenOrCreate);
        PlayerData playerData = (PlayerData)bf.Deserialize(file);
        initWithPlayerData(playerData, false);
        bf.Serialize(file, playerData);
        file.Close();
    }

    public void initWithPlayerData(PlayerData playerData, bool forceSave = true)
    {
        if (playerData.coins != _coins)
        {
            _coins = playerData.coins;
            if (CoinsDidChangeEvent != null)
            {
                CoinsDidChangeEvent();
            }
        }
        _powerFindWordsNumber = playerData.powerFindWordsNumber;
        if (forceSave)
        {
            Save();
        }
    }

    public static IEnumerator waitForGameControl()
    {
        while (GameControl.instance == null ||
        GameControl.instance.IsReady == false)
        {
            yield return new WaitForSeconds(.1f);
        }
    }

    public static IEnumerator waitForConnectionTracker()
    {
        while (GameControl.instance == null ||
        GameControl.instance.connectionTracker == null ||
        GameControl.instance.connectionTracker.IsReady == false)
        {
            yield return new WaitForSeconds(.1f);
        }
    }

	public static IEnumerator waitForWordsChecker()
	{
		while (GameControl.instance == null ||
			GameControl.instance.wordsChecker == null ||
			GameControl.instance.wordsChecker.IsLoaded == false)
		{
			yield return new WaitForSeconds(.1f);
		}
	}

    public PlayerData getPlayerData()
    {
        PlayerData playerData = new PlayerData();
        playerData.coins = this.Coins;
        playerData.powerFindWordsNumber = this.PowerFindWordsNumber;

        return playerData;
    }
}


[Serializable]
public class PlayerData
{
    public int coins;
    public int powerFindWordsNumber;
}
