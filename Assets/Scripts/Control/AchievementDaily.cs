﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class AchievementDaily : MonoBehaviour {
    public int currentStreak = 0;
    public int bestStreak = 0;
    public bool IsReady = false;

	IEnumerator Start () {
        Load();
        yield return GameControl.waitForConnectionTracker();
        DateTime lastConnectionDate = GameControl.instance.connectionTracker.lastConnectionDate;

        // FIXME: first day of the year, this will count as a daily streak (I don't care).
        if (lastConnectionDate.Year < DateTime.Now.Year || lastConnectionDate.DayOfYear + 1 == DateTime.Now.DayOfYear)
        {
            currentStreak++;
            if (currentStreak > bestStreak)
            {
                bestStreak = currentStreak;
            }
            print("Current streak improved !");
            yield return GameControl.waitForGameControl();
            GameControl.instance.Coins += 10;
        } else if (lastConnectionDate.DayOfYear == DateTime.Now.DayOfYear)
        {
            print("Already Connected today.");
        } else
        {
            print("Current streak broken...");
            currentStreak = 1;
        }
        Save();
        GameControl.instance.connectionTracker.UpdateLastConnectionDate();
        IsReady = true;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/achievementDailyInfo.dat", FileMode.OpenOrCreate);
        AchievementDailyData achievementDailyData = new AchievementDailyData();
        achievementDailyData.bestStreak = this.bestStreak;
        achievementDailyData.bestStreak = this.bestStreak;
        bf.Serialize(file, achievementDailyData);
        file.Close();
    }

    void Load()
    {
        if (!File.Exists(Application.persistentDataPath + "/achievementDailyInfo.dat"))
        {
            print("Can't load");
            IsReady = true;
            return;
        }
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/achievementDailyInfo.dat", FileMode.OpenOrCreate);
        AchievementDailyData achievementDailyData = (AchievementDailyData)bf.Deserialize(file);

        currentStreak = achievementDailyData.currentStreak;
        bestStreak = achievementDailyData.bestStreak;
        bf.Serialize(file, achievementDailyData);
        file.Close();
        IsReady = true;
    }
}

[Serializable]
class AchievementDailyData
{
    public int currentStreak;
    public int bestStreak;
}