﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerFindWord : APower {

    public RectTransform selectedLettersContainer;
    public GameManager gameManager;
    public LetterHint letterHintPrefab;
    RectTransform ghostContainer;
    string givenWord = null;
    //RectTransform 

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public override void Activate(List<ARunner> runners)
    {
        base.Activate(runners);
        if (GameControl.instance.PowerFindWordsNumber <= 0)
        {
            return;
        }
        gameManager.GameDidValidateWordEvent += gameDidValidateWord;
        List<string> wordsLeftToGuess = gameManager.GetWordsLeftToGuess();
        if (wordsLeftToGuess.Count <= 0 || givenWord != null)
        {
            return;
        }
        MovableLetter[] movableLetters = selectedLettersContainer.GetComponentsInChildren<MovableLetter>();
        foreach (MovableLetter ml in movableLetters)
        {
            ml.Select();
        }
        if (ghostContainer != null)
        {
            Destroy(ghostContainer.gameObject);
        }
        ghostContainer = Instantiate(selectedLettersContainer, selectedLettersContainer.parent, true);
        ghostContainer.name = "ghostContainer";
        selectedLettersContainer.SetAsLastSibling();

        givenWord = wordsLeftToGuess[Random.Range(0, wordsLeftToGuess.Count - 1)].ToUpper();
        foreach(char c in givenWord)
        {
            LetterHint letterHint = GameObject.Instantiate(letterHintPrefab);
            letterHint.SetLetter(c);
            letterHint.transform.SetParent(ghostContainer);
        }
        GameControl.instance.PowerFindWordsNumber--;
        print("powerFindWords : " + GameControl.instance.PowerFindWordsNumber);
    }
    void gameDidValidateWord(string validatedWord)
    {
        if (givenWord != null  && validatedWord == givenWord.ToLower())
        {
            gameManager.GameDidValidateWordEvent -= gameDidValidateWord;
            // FIXME: Destroy in loop, not very good practice
            foreach (Transform childTransform in ghostContainer.transform)
            {
                Destroy(childTransform.gameObject);
            }
            givenWord = null;
        }
    }
}
